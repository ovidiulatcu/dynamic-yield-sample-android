package eu.corebuild.dytest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;

import com.dynamicyield.dyapi.DYApi;
import com.dynamicyield.engine.DYEngine;
import com.dynamicyield.engine.DYPageContext;
import com.dynamicyield.listener.itf.DYListenerItf;
import com.dynamicyield.state.DYExperimentsState;
import com.dynamicyield.state.DYInitState;

import org.json.JSONArray;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "DYListenerITF";
    public static final String SECRET_KEY = "//TODO:replace with secret key";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setupDY();
        findViewById(R.id.triggerLoadWeb).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadSmartObjWeb();
            }
        });

        findViewById(R.id.triggerLoadImage).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadSmartObjImage();
            }
        });

        findViewById(R.id.trackPageView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                trackHomePage();
            }
        });

        findViewById(R.id.trackAddToCart).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                trackAddToCart();
            }
        });

        findViewById(R.id.trackCustomEvent).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                trackCustomEvent();
            }
        });
        findViewById(R.id.trackProductView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                trackProductView();
            }
        });
        findViewById(R.id.trackPurchase).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                trackPurchase();
            }
        });
    }

    private void trackHomePage() {
        DYPageContext pageContext = new DYPageContext("en_US", DYPageContext.HOMEPAGE, null);
        DYApi.getInstance().trackPageView("dynamic4", pageContext);
    }

    private void loadSmartObjWeb() {
        WebView webView = findViewById(R.id.webview);
        DYApi.getInstance().loadSmartObject(webView, "dynamic4", "http://google.com");
    }

    private void loadSmartObjImage() {
        DYApi.getInstance().loadSmartObject((ImageView) findViewById(R.id.imageView),
                "dynamic4", "https://i.pinimg.com/originals/e8/b3/a0/e8b3a0244b14d5563b3868da15bec8f7.jpg");
    }

    private void trackAddToCart() {
        try {
            JSONObject props = new JSONObject();
            props.put("value", 103.81);
            props.put("productId", "3814TT82033105");
            props.put("quantity", 1);
            props.put("dyType", "add-to-cart-v1");
            DYApi.getInstance().trackEvent("Add to Cart", props);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void trackCustomEvent() {
        JSONObject props = new JSONObject();
        DYApi.getInstance().trackEvent("custom_event_test", props);
    }

    private void trackProductView() {
        JSONArray data = new JSONArray();
        data.put("SKU-1512512512");
        DYPageContext pageContext = new DYPageContext("en_US", DYPageContext.PRODUCT, data);
        DYApi.getInstance().trackPageView("SKU-1512512512", pageContext);
    }

    private void trackPurchase() {
        try {
            //Populating the Prop parameter
            JSONArray cart = new JSONArray();
            JSONObject product = new JSONObject();

            product.put("productId", "3814TT82033105");
            product.put("quantity", 1);
            product.put("itemPrice", 100);

            cart.put(product);

            JSONObject purchase = new JSONObject();

            purchase.put("dyType", "purchase-v1");
            purchase.put("value", 100);
            purchase.put("cart", cart);

            DYApi.getInstance().trackEvent("purchase", purchase);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setupDY() {
        DYApi.setContextAndSecret(getApplication(), SECRET_KEY);
        DYApi.getInstance().enableDeveloperLogs(true);


        DYListenerItf listenerItf = new DYListenerItf() {
            @Override
            public void experimentsReadyWithState(DYExperimentsState dyExperimentsState) {
                Log.i(TAG, "experimentsReadyWithState " + dyExperimentsState.toString());
            }

            @Override
            public void experimentsUpdatedAndReady() {
                Log.i(TAG, "expermentUpdatedAndReady");
            }

            @Override
            public void initReply(DYInitState dyInitState) {
                Log.i(TAG, "initReply " + dyInitState);
            }

            @Override
            public void onSmartObjectLoadResult(String s, String s1, View view) {
                Log.i(TAG, "onSmartObjectLoad" + s + s1);
            }

            @Override
            public void onSmartActionReturned(String s, JSONObject jsonObject) {
                Log.i(TAG, "onSmartActionReturned" + jsonObject.toString());
            }

            @Override
            public boolean shouldDYRender(String s) {
                return true;
            }
        };
        DYApi.getInstance().setListener(listenerItf);
    }
}
